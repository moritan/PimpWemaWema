

function addEvent(obj, event, fct) {
    if (obj.attachEvent) //Est-ce IE ?
        obj.attachEvent("on" + event, fct); //Ne pas oublier le "on"
    else
        obj.addEventListener(event, fct, true);
};


function init_slid() {  

    var slider = document.getElementById('sliderRegular');
    
    noUiSlider.create(slider, {
        start: 40,
        connect: "lower",
        range: {
            min: 0,
            max: 100
        }
    });

    var slider2 = document.getElementById('sliderRegular2');
    
    noUiSlider.create(slider2, {
        start: 50,
        connect: "lower",
        range: {
            min: 0,
            max: 100
        }
    });

    var slider3 = document.getElementById('sliderRegular3');
    
    noUiSlider.create(slider3, {
        start: 20,
        connect: "lower",
        range: {
            min: 0,
            max: 100
        }
    });
    
 } ;

 
addEvent(window , "load", init_slid); 


